import numpy as np
import tensorflow as tf
import codecs
import os


class TrainConf:
    """Train config"""
    TRAIN = True
    HIDDEN = 512  # hidden size
    LR = 2e-3
    DROP = 0.5  # Dropout prob
    M_GRAD = 5.  # Max grad norm
    BATCH = 100  # Batch size
    L = 100  # Sequence length
    LAYERS = 3  # Number os LSTM Layer
    TEMP = 0.5  # Temperature of inference step. Range (0..1]. Higher value
    # makes the reference for rigid.


class ValConf(TrainConf):
    """Validation Config"""
    TRAIN = False


class InfConf(TrainConf):
    """Inference Config"""
    TRAIN = False
    BATCH = 1
    L = 1


class Data:
    def __init__(self, file_name, conf):
        """
        :param file_name:
        :param conf: config class
        :type conf: TrainConf
        """
        self.data = codecs.open(file_name, mode='r', encoding='utf-8').read()
        chars = list(set(self.data))
        self.N_DATA, self.N_VOCAB = len(self.data), len(chars)
        self.N_TRAIN = int((self.N_DATA / 100) * 99)
        self.char2ix = {ch: i for i, ch in enumerate(chars)}
        self.ix2char = {i: ch for i, ch in enumerate(chars)}
        self.CONF = conf

    def c2i(self, char):
        return self.char2ix[char]

    def i2c(self, index):
        return self.ix2char[index]

    def cvrt2hot(self, text):
        """Convert string to one hot vector"""
        size = len(text)
        result = np.zeros((size, self.N_VOCAB))
        for i in range(size):
            index = self.c2i(text[i])
            result[i, index] = 1.
        return result

    def batch_iterator(self, text, batch_size, seq_len):
        batch_distance = len(text) / batch_size
        n_iterate = int((batch_distance - 1) / seq_len)
        for i in range(n_iterate):
            x, y = [], []
            for j in range(batch_size):
                start = int(j * batch_distance + i * seq_len)
                end = int(start + seq_len)
                cook = self.cvrt2hot(text[start:end + 1])
                x.append(cook[0:-1])
                y.append(cook[1:])
            yield(x, y)

    def train_iterator(self, batch_size=None, seq_len=None):
        batch_size = self.CONF.BATCH if batch_size is None else batch_size
        seq_len = self.CONF.L if seq_len is None else seq_len
        return self.batch_iterator(self.data[:self.N_TRAIN],
                                   batch_size,
                                   seq_len)

    def val_iterator(self, batch_size=None, seq_len=None):
        batch_size = self.CONF.BATCH if batch_size is None else batch_size
        seq_len = self.CONF.L if seq_len is None else seq_len
        return self.batch_iterator(self.data[self.N_TRAIN:],
                                   batch_size,
                                   seq_len)


class Graph(object):
    def __init__(self, conf, data):
        """
        :param conf:
        :type conf: TrainConf
        :param data:
        :type data: Data
        """
        initializer = tf.random_uniform_initializer(-0.01, 0.01)
        reuse = None if conf.TRAIN else True

        with tf.variable_scope("model", reuse=reuse, initializer=initializer):
            data_shape = [conf.BATCH, conf.L, data.N_VOCAB]
            inputs = tf.placeholder(tf.float32, data_shape)
            targets = tf.placeholder(tf.float32, data_shape)

            cell = tf.nn.rnn_cell.BasicLSTMCell(conf.HIDDEN)
            if conf.TRAIN and conf.DROP < 1:
                cell = tf.nn.rnn_cell.DropoutWrapper(cell,
                                                     output_keep_prob=conf.DROP)
            cell = tf.nn.rnn_cell.MultiRNNCell([cell] * conf.LAYERS)
            initial_state = cell.zero_state(conf.BATCH, tf.float32)
            state = initial_state

            temper = tf.Variable(conf.TEMP)

            chain_output = []
            rnn_output = None
            with tf.variable_scope("RNN"):
                for step in range(conf.L):
                    if step > 0:
                        tf.get_variable_scope().reuse_variables()

                    step_inputs = inputs[:, step, :]

                    (cell_output, state) = cell(step_inputs, state)
                    chain_output.append(cell_output)

                    if step == conf.L - 1:
                        rnn_output = cell_output

            softmax_w = tf.get_variable("softmax_w",
                                        [conf.HIDDEN,
                                         data.N_VOCAB])
            softmax_b = tf.get_variable("softmax_b",
                                        [data.N_VOCAB])
            chain_output = tf.reshape(tf.concat(1, chain_output),
                                      [-1, conf.HIDDEN])

            logits = tf.matmul(chain_output, softmax_w) + softmax_b

            cost_func = tf.nn.softmax_cross_entropy_with_logits
            loss = cost_func(logits, tf.reshape(targets, [-1, data.N_VOCAB]))
            loss = tf.reduce_sum(loss) / (conf.BATCH * conf.L)

            output = tf.matmul(rnn_output, softmax_w) + softmax_b
            probs = tf.nn.softmax(output/temper)

            variables = tf.trainable_variables()
            # All trainable variable from the graph
            global_step = tf.Variable(0)
            learning_rate = tf.train.exponential_decay(conf.LR, global_step,
                                                       3000, 0.97,
                                                       staircase=True)

            gradients = tf.gradients(loss, variables)
            gradients = tf.clip_by_global_norm(gradients, conf.M_GRAD)[0]

            optimizer = tf.train.RMSPropOptimizer(learning_rate, decay=0.95)
            train_step = optimizer.apply_gradients(zip(gradients, variables),
                                                   global_step=global_step)

        self.out_put = rnn_output
        self.stateIn = initial_state
        self.stateOut = state
        self.loss = loss
        self.train_step = train_step
        self.inputs = inputs
        self.targets = targets
        self.logits = logits
        self.grads = gradients
        self.probs = probs
        self.learning_rate = learning_rate
        self.global_step = global_step
        self.temper = temper


class LSTMBot(object):
    def __init__(self, file_name):
        self.file_name = file_name
        self.data = Data(self.file_name, TrainConf)
        self.TrnGraph = Graph(TrainConf, self.data)
        self.infGraph = Graph(InfConf, self.data)
        self.session = tf.Session()
        tf.initialize_all_variables().run(session=self.session)

        self.dir_name = os.path.splitext(self.file_name)[0]
        self.saver = tf.train.Saver()
        self.epoch = 0
        if not os.path.exists(self.dir_name):
            os.makedirs(self.dir_name)
        checkpoint = tf.train.latest_checkpoint(self.dir_name)
        if checkpoint:
            self.epoch = int(checkpoint.split('_')[-1])
            self.saver.restore(self.session, checkpoint)

    def __del__(self):
        self.session.close()

    def sample(self, seed, length, temper=None):
        result = seed
        seed += u" "
        history = self.session.run(self.infGraph.stateIn)
        temper = InfConf.TEMP if temper is None else temper
        # Temperature to season the generated sentence
        probs = None
        # Probability to be the next character of all the character

        for (x, y) in self.data.batch_iterator(seed, 1, 1):
            t_result = self.session.run([self.infGraph.stateOut,
                                         self.infGraph.probs],
                                        {self.infGraph.inputs: x,
                                         self.infGraph.stateIn: history,
                                         self.infGraph.temper: temper})
            history, probs = t_result

        for i in range(length):
            index = np.random.choice(self.data.N_VOCAB, p=probs.ravel())
            inputs = np.zeros((1, 1, self.data.N_VOCAB))
            inputs[0,0,index] = 1.
            result += self.data.i2c(index)

            if i < length - 1:
                t_result = self.session.run([self.infGraph.stateOut,
                                             self.infGraph.probs],
                                            {self.infGraph.inputs: inputs,
                                             self.infGraph.temper: temper,
                                             self.infGraph.stateIn: history})
                history, probs = t_result

        return result

    def validate(self):
        history = self.session.run(self.TrnGraph.stateIn)
        losses = []
        for k, (inputs, targets) in enumerate(self.data.val_iterator()):
            if k > 30: break
            t_result = self.session.run([self.TrnGraph.stateOut,
                                         self.TrnGraph.loss],
                                        {self.TrnGraph.inputs: inputs,
                                         self.TrnGraph.targets: targets,
                                         self.TrnGraph.stateIn: history})
            history, val_loss = t_result
            losses.append(val_loss)
        return np.array(losses).mean()

    def train(self, n_epoch=50):
        for epoch in range(self.epoch, n_epoch + 1):
            save_path = self.dir_name + "/epoch_%d" % epoch
            if epoch % 5 == 0 and not os.path.exists(save_path):
                self.saver.save(self.session,  save_path)

            # Create blank state each new epoch
            history = self.session.run(self.TrnGraph.stateIn)

            # Loop though the sequence and train all the batch at once
            for (x, y) in self.data.train_iterator():
                t_result = self.session.run([self.TrnGraph.stateOut,
                                             self.TrnGraph.loss,
                                             self.TrnGraph.learning_rate,
                                             self.TrnGraph.global_step,
                                             self.TrnGraph.train_step],
                                            {self.TrnGraph.inputs: x,
                                             self.TrnGraph.stateIn: history,
                                             self.TrnGraph.targets: y})
                history, train_loss, train_lr, count, _ = t_result

                if count % 100 == 0:
                    val_loss = self.validate()
                    sample = self.sample(u'The hero', 40)

                    print("Epoch %d Iterate %d loss %.3E val %.3E %s" % (
                        epoch, count, train_loss, val_loss, sample.replace(
                            "\n", "<EOL>")))

if __name__ == "__main__":
    bot = LSTMBot("wap.txt")
    bot.train()
