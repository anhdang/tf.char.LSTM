This is my TensorFlow implementation of character level LSTM network for the 
Language Modeling task. The original code was created several months ago and 
updated recently to make it work on TensorFlow r0.12. For educational purpose, 
I keep the code as plain as possible so you can tinker it and run as it is.

### Requirement

- Python 3.5
- Numpy
- TensorFlow (r0.12)

### How to run

`$ python main.py`

With the default setting, following is the result of my TITAN X (Not the Pascal 
one) after about 13 minutes of training:

```
Epoch 0 Iterate 100 loss 4.528E+00 val 4.526E+00 The heroMà'XjCU*:PAî9êZ)6.âpf7Z.À6"iVàúá9àEX;:Ah
Epoch 0 Iterate 200 loss 3.405E+00 val 3.258E+00 The hero0eÀ h  eee   e a  i  e ee   e ea  e  i  
Epoch 0 Iterate 300 loss 3.096E+00 val 3.014E+00 The heroan ra s eteit     e yit eth rst oooe oae
Epoch 1 Iterate 400 loss 3.095E+00 val 3.015E+00 The heroit tn aostt  atiran o  a e mr  roargrhtn
Epoch 1 Iterate 500 loss 3.092E+00 val 3.000E+00 The heroe    tnt h  n  ei h hireei n  e n aet a 
Epoch 1 Iterate 600 loss 3.078E+00 val 2.996E+00 The heroeeaate  net    ed h e e   o   e   e  ete
Epoch 2 Iterate 700 loss 2.857E+00 val 2.772E+00 The hero fhe ah wett roe dar wh tr ah to oe ahn 
Epoch 2 Iterate 800 loss 2.466E+00 val 2.457E+00 The herood on tha her nhe seunnd ind tere fho sa
Epoch 2 Iterate 900 loss 2.252E+00 val 2.779E+00 The heror has hem begesent thet in thet on thet 
Epoch 3 Iterate 1000 loss 2.044E+00 val 2.094E+00 The hero as the file and the dore in the was the
Epoch 3 Iterate 1100 loss 1.863E+00 val 1.971E+00 The herong some the solling which and his somemi
Epoch 3 Iterate 1200 loss 1.887E+00 val 2.084E+00 The herong, pist in thet wes to thet wes before 
Epoch 4 Iterate 1300 loss 1.691E+00 val 1.780E+00 The hero and who would be deeped to the looked a
Epoch 4 Iterate 1400 loss 1.593E+00 val 1.716E+00 The heros and a same said was find with his hand
Epoch 4 Iterate 1500 loss 1.604E+00 val 1.678E+00 The hero dound the well and speaked him in the c
Epoch 5 Iterate 1600 loss 1.491E+00 val 1.616E+00 The hero was a pleasing the room and a dirlle an
Epoch 5 Iterate 1700 loss 1.499E+00 val 1.624E+00 The herorely again the army the officers mast fr
Epoch 5 Iterate 1800 loss 1.431E+00 val 1.566E+00 The hero and the captain of the Russian had had 
Epoch 6 Iterate 1900 loss 1.423E+00 val 1.519E+00 The hero and the battle and the given of the str
Epoch 6 Iterate 2000 loss 1.389E+00 val 1.523E+00 The hero and this countess of some in the same m
Epoch 6 Iterate 2100 loss 1.346E+00 val 1.504E+00 The hero and seemed to the French still and that
Epoch 6 Iterate 2200 loss 1.341E+00 val 1.479E+00 The hero and the countess went on to him the sol
Epoch 7 Iterate 2300 loss 1.368E+00 val 1.479E+00 The hero at the princess were lightly as matter 
Epoch 7 Iterate 2400 loss 1.315E+00 val 1.458E+00 The hero and the same commander of a mistake and
Epoch 7 Iterate 2500 loss 1.331E+00 val 1.432E+00 The hero began to stay it was began to say to hi
Epoch 8 Iterate 2600 loss 1.325E+00 val 1.436E+00 The hero were replied to the respect of the fron
Epoch 8 Iterate 2700 loss 1.316E+00 val 1.434E+00 The hero conversation and the sound of the count
Epoch 8 Iterate 2800 loss 1.255E+00 val 1.434E+00 The hero in the brother was not going to his eye
Epoch 9 Iterate 2900 loss 1.283E+00 val 1.416E+00 The hero so that the man came, and her cross to 
Epoch 9 Iterate 3000 loss 1.252E+00 val 1.407E+00 The hero in Moscow with a presence and the same 
Epoch 9 Iterate 3100 loss 1.271E+00 val 1.398E+00 The hero and the thought of the day was the prin
Epoch 10 Iterate 3200 loss 1.253E+00 val 1.388E+00 The hero with the mistake had been the disconten
Epoch 10 Iterate 3300 loss 1.240E+00 val 1.396E+00 The hero are as the hands, and the hands of the
```